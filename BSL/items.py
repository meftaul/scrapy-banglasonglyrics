# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BslItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class Category(scrapy.Item):
    # Category name an link definition
    name = scrapy.Field()
    link = scrapy.Field()


class Song(scrapy.Item):
    title = scrapy.Field()
    bsl_link = scrapy.Field()
    youtube_link = scrapy.Field()
    lyrics = scrapy.Field()
    song_detail = scrapy.Field()

