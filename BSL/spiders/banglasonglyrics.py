# -*- coding: utf-8 -*-
import scrapy
from BSL.items import Song


class BanglasonglyricsSpider(scrapy.Spider):
    name = 'bsl'
    allowed_domains = ['banglasonglyrics.com']
    start_urls = ['http://banglasonglyrics.com/']

    # def parse(self, response):
    #     for sel in response.xpath("id('categories-6')/ul/li/a"):
    #         item = Category()
    #         item['name'] = sel.xpath('text()').extract_first()
    #         item['link'] = sel.xpath('@href').extract_first()
    #
    #         yield item

    def parse(self, response):
        for sel in response.xpath("//div[@class='card-block']/h4/a"):
            links = sel.xpath('@href').extract()
            for link in links:
                song_link = response.urljoin(link)
                #print(song_link)
                yield scrapy.Request(song_link, callback=self.parse_item)

        next_page = response.xpath("//a[@class='next page-numbers']/@href").extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            print(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_item(self, response):
        print('Parsing Item :: {0}'.format(response.url))
        item = Song()
        item['title'] = response.xpath("//h1[@class='entry-title']/text()").extract_first()
        item['bsl_link'] = response.url
        item['lyrics'] = response.css('div.lyrics-content').extract_first()
        item['song_detail'] = response.xpath("//div[@class='song-detail']/ul").extract_first()
        item['youtube_link'] = response.xpath("//div[@class='col-md-6']/div[@class='entry-media embed-responsive']").extract_first()

        yield item
