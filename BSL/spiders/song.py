# -*- coding: utf-8 -*-
import scrapy

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from BSL.items import Song
from bs4 import BeautifulSoup


class SongSpider(CrawlSpider):
    name = 'song'
    allowed_domains = ['banglasonglyrics.com']
    start_urls = ['https://banglasonglyrics.com/']

    rules = (
        Rule(LinkExtractor(allow=(r'https://banglasonglyrics.com/category/.*/', )),
             callback='parse_category',
             follow=True),
        Rule(LinkExtractor(allow=(r'https://banglasonglyrics.com/category/.*/page/d+/',)),
             callback='parse_category',
             follow=True),
        Rule(LinkExtractor(allow=(r'https://banglasonglyrics.com/page/d+/',)),
             callback='parse_page',
             follow=True),
        Rule(LinkExtractor(allow=("\.*banglasonglyrics.com/\d+/.*", )),
             callback='parse_item'),
    )

    def parse_page(self, response):
        print('Parsing Page:: {0}'.format(response.url))


    def parse_category(self, response):
        print('Parsing Category:: {0}'.format(response.url))

    def parse_item(self, response):
        print('Parsing Item :: {0}'.format(response.url))
        item = Song()
        item['title'] = response.xpath("//h1[@class='entry-title']/text()").extract_first()
        item['bsl_link'] = response.url
        item['lyrics'] = response.css('div.lyrics-content').extract_first()
        item['song_detail'] = response.xpath("//div[@class='song-detail']/ul").extract_first()
        item['youtube_link'] = response.xpath("//div[@class='col-md-6']/div[@class='entry-media embed-responsive']").extract_first()

        yield item

    def get_clean_text(self, raw_sring):
        soup = BeautifulSoup(raw_sring)
        return soup.get_text()


    # def parse(self, response):
    #     print('Parsing Some url :: {0}'.format(response.url))
